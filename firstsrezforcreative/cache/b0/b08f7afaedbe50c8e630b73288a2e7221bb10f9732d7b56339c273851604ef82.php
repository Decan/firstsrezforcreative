<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.html.twig */
class __TwigTemplate_685fd3ba8620ba056c23b4b9fbcba4595b42c8c0a4d3bec811d713813c1d58ea extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 2
        echo twig_escape_filter($this->env, (((isset($context["lang"]) || array_key_exists("lang", $context))) ? (_twig_default_filter(($context["lang"] ?? null), "en")) : ("en")), "html", null, true);
        echo "\">
<head>
  <meta charset=\"UTF-8\">
  <title>";
        // line 5
        echo twig_escape_filter($this->env, (((isset($context["title"]) || array_key_exists("title", $context))) ? (_twig_default_filter(($context["title"] ?? null), "Welcome")) : ("Welcome")), "html", null, true);
        echo "</title>
  <link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css\">
  <style type=\"text/css\">
    body {
      font-family: sans-serif;
      font-size: .9em;
    }

    main.container {
      padding: 2em;
    }
    h3 {
      font-size: 1.2em;
      padding: 1em 0 .5em 0;
    }

    pre {
      font-family: monospace;
      padding: 1rem;
      background-color: #fafafa;
      border-radius: .2rem;
      border: solid 1px #f9f9f9;
      box-shadow: inset 0 0 12px 0 #f9f9f9;
      line-height: 1.5;
    }

    pre.dark {
      box-shadow: none;
      background-color: #000;
      color: #00ff00;
    }
  </style>
  <script type=\"text/javascript\">
    (() => {
      window.addEventListener('DOMContentLoaded', () => {
        [].forEach.call(document.querySelectorAll('pre'), (element) => {
          element.addEventListener('mouseover', (e) => {
            e.target.classList.add('dark');
          });
          element.addEventListener('mouseout', (e) => {
            e.target.classList.remove('dark')
          });
        })
      });
    })()
  </script>
</head>
<body>
<main class=\"container\">
  <article>
    <h3>Environment variables</h3>
    <pre>";
        // line 56
        echo ($context["env_vars"] ?? null);
        echo "</pre>

    <h3>Server</h3>
    <pre>";
        // line 59
        echo ($context["server"] ?? null);
        echo "</pre>

    <h3>Headers</h3>
    <pre>";
        // line 62
        echo ($context["headers"] ?? null);
        echo "</pre>

    <h3>Get</h3>
    <pre>";
        // line 65
        echo ($context["get"] ?? null);
        echo "</pre>

    <h3>Post</h3>
    <pre>";
        // line 68
        echo ($context["post"] ?? null);
        echo "</pre>
  </article>
</main>

</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 68,  118 => 65,  112 => 62,  106 => 59,  100 => 56,  46 => 5,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"{{ lang | default('en') }}\">
<head>
  <meta charset=\"UTF-8\">
  <title>{{ title | default('Welcome') }}</title>
  <link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css\">
  <style type=\"text/css\">
    body {
      font-family: sans-serif;
      font-size: .9em;
    }

    main.container {
      padding: 2em;
    }
    h3 {
      font-size: 1.2em;
      padding: 1em 0 .5em 0;
    }

    pre {
      font-family: monospace;
      padding: 1rem;
      background-color: #fafafa;
      border-radius: .2rem;
      border: solid 1px #f9f9f9;
      box-shadow: inset 0 0 12px 0 #f9f9f9;
      line-height: 1.5;
    }

    pre.dark {
      box-shadow: none;
      background-color: #000;
      color: #00ff00;
    }
  </style>
  <script type=\"text/javascript\">
    (() => {
      window.addEventListener('DOMContentLoaded', () => {
        [].forEach.call(document.querySelectorAll('pre'), (element) => {
          element.addEventListener('mouseover', (e) => {
            e.target.classList.add('dark');
          });
          element.addEventListener('mouseout', (e) => {
            e.target.classList.remove('dark')
          });
        })
      });
    })()
  </script>
</head>
<body>
<main class=\"container\">
  <article>
    <h3>Environment variables</h3>
    <pre>{{ env_vars | raw }}</pre>

    <h3>Server</h3>
    <pre>{{ server | raw }}</pre>

    <h3>Headers</h3>
    <pre>{{ headers | raw }}</pre>

    <h3>Get</h3>
    <pre>{{ get | raw }}</pre>

    <h3>Post</h3>
    <pre>{{ post | raw }}</pre>
  </article>
</main>

</body>
</html>
", "index.html.twig", "/var/www/app/templates/index.html.twig");
    }
}
